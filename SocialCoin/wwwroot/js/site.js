﻿var i18n = new VueI18n({
    locale: 'en',
    fallBackLocale: 'en',
    messages: {
        en: {
            hello: 'Hello EN'
        },
        nl: {
            hello: 'Hallo NL'
        }
    }
})
var app = new Vue({
    el: '#app',
    i18n
})
function changeLang(lang) {
    this.i18n.locale = lang
}