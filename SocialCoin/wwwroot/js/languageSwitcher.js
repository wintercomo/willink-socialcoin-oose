﻿export default {
    name: 'LanguageSwitcher',
    data: () => ({ languages: ['en', 'nl'] }),
    template: '<ul><li v-for="lang in languages" :key="lang" @click="changeLang(lang)">{{ lang }}</li></ul>',
    methods: {
        changeLang(lang) {
            this.i18n.locale = lang
        }
    }
}